import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Ex2 {
    private JFrame jFrame;
    private JButton jButton;
    protected JTextField jTextField;
    protected JTextArea jTextArea;
    public Ex2(){
        this.jFrame=new JFrame();
        this.jButton=new JButton("BUTON");
        this.jTextField=new JTextField();
        this.jTextArea= new JTextArea();
        this.jFrame.setSize(500,500);
        this.jFrame.setLayout(null);
        this.jTextField.setBounds(5,20,100,20);
        this.jTextArea.setBounds(5,130,300,40);
        this.jButton.setBounds(5,240,120,60);
        this.jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String numefisier=jTextField.getText();
                try {
                    BufferedReader read=new BufferedReader(new FileReader(numefisier));//in JTextField trebuie introdus numele complet al fisierului
                                                                                        //incluzand extensia".txt"
                    jTextArea.setText(read.readLine());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.jFrame.add(this.jTextArea);
        this.jFrame.add(this.jButton);
        this.jFrame.add(this.jTextField);
        this.jFrame.setVisible(true);
    }

    public static void main(String[] args) {
        Ex2 ex2= new Ex2();
    }
}